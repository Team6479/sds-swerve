package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TeleopDrive extends CommandBase {
    private final Drivetrain m_drivetrainSubsystem;

    private final Supplier<Double> m_translationXSupplier;
    private final Supplier<Double> m_translationYSupplier;
    private final Supplier<Double> m_rotationSupplier;

    public TeleopDrive(Drivetrain drivetrainSubsystem,
                               Supplier<Double> translationXSupplier,
                               Supplier<Double> translationYSupplier,
                               Supplier<Double> rotationSupplier) {
        this.m_drivetrainSubsystem = drivetrainSubsystem;
        this.m_translationXSupplier = translationXSupplier;
        this.m_translationYSupplier = translationYSupplier;
        this.m_rotationSupplier = rotationSupplier;

        addRequirements(drivetrainSubsystem);
    }

    @Override
    public void execute() {
        // You can use `new ChassisSpeeds(...)` for robot-oriented movement instead of field-oriented movement
        m_drivetrainSubsystem.drive(
                ChassisSpeeds.fromFieldRelativeSpeeds(
                        -modifyAxis(m_translationXSupplier.get()) * Drivetrain.MAX_VELOCITY_METERS_PER_SECOND,
                        -modifyAxis(m_translationYSupplier.get()) * Drivetrain.MAX_VELOCITY_METERS_PER_SECOND,
                        -modifyAxis(m_rotationSupplier.get()) * Drivetrain.MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND,
                        m_drivetrainSubsystem.getGyroscopeRotation()
                )
        );
    }

    private static double deadband(double value, double deadband) {
        if (Math.abs(value) > deadband) {
          if (value > 0.0) {
            return (value - deadband) / (1.0 - deadband);
          } else {
            return (value + deadband) / (1.0 - deadband);
          }
        } else {
          return 0.0;
        }
      }
    
      private static double modifyAxis(double value) {
        // Deadband
        value = deadband(value, 0.05);
    
        // Square the axis
        value = Math.copySign(value * value, value);
    
        return value;
      }

    @Override
    public void end(boolean interrupted) {
        m_drivetrainSubsystem.drive(new ChassisSpeeds(0.0, 0.0, 0.0));
    }
}