// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.autos;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public abstract class RecordedAuto extends CommandBase {
  
  private Drivetrain drivetrain;

  protected double[][] points;
  private int index;
  private double startTime;

  /** Creates a new RecordedAuto. */
  public RecordedAuto(Drivetrain drivetrain) {
    this.drivetrain = drivetrain;
    // This should be overridden in subclasses
    this.points = new double[0][0];
    this.index = 0;
    this.startTime = 0;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(drivetrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    drivetrain.drive(new ChassisSpeeds(0.0, 0.0, 0.0));
    this.startTime = Timer.getFPGATimestamp();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if((points[index][0]) < (Timer.getFPGATimestamp() - startTime) && index < points.length-1) {

      drivetrain.drive(
              ChassisSpeeds.fromFieldRelativeSpeeds(
                      -modifyAxis(points[index][2]) * Drivetrain.MAX_VELOCITY_METERS_PER_SECOND,
                      -modifyAxis(points[index][1]) * Drivetrain.MAX_VELOCITY_METERS_PER_SECOND,
                      -modifyAxis(points[index][3]) * Drivetrain.MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND,
                      drivetrain.getGyroscopeRotation()
              )
      );
      index++;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drivetrain.drive(new ChassisSpeeds(0.0, 0.0, 0.0));
    DriverStation.reportError("finished", false);
  }

  private static double deadband(double value, double deadband) {
    if (Math.abs(value) > deadband) {
      if (value > 0.0) {
        return (value - deadband) / (1.0 - deadband);
      } else {
        return (value + deadband) / (1.0 - deadband);
      }
    } else {
      return 0.0;
    }
  }

  private static double modifyAxis(double value) {
    // Deadband
    value = deadband(value, 0.05);

    // Square the axis
    value = Math.copySign(value * value, value);

    return value;
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return index >= points.length-1;
  }
}
