package frc.robot.autos;

import frc.robot.subsystems.Drivetrain;

public class TemplateRecordedAuto extends RecordedAuto {
    public TemplateRecordedAuto(Drivetrain drivetrain) {
        super(drivetrain);
        super.points = new double[][]{
            // timestamp, leftx, lefty, rightx
            {0,0,0,0},
            {0,0,0,0}
        };
    }
}
