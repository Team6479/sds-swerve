// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.team6479.lib.controllers.CBXboxController;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.autos.NewRecording;
import frc.robot.commands.TeleopDrive;
import frc.robot.subsystems.Drivetrain;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  private boolean recordAuto = true;
  private ArrayList<ArrayList<Double>> recording = new ArrayList<ArrayList<Double>>();
  private double timerOffset;

  private final CBXboxController xbox = new CBXboxController(0);

  private final Drivetrain drivetrain = new Drivetrain();

  private boolean justDidTeleop = false;
  private boolean hasBegunRecording = false;


  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    drivetrain.setDefaultCommand(new TeleopDrive(
            drivetrain,
            xbox::getLeftY,
            xbox::getLeftX,
            xbox::getRightX
    ));
  }

  public void teleopInit() {
    this.justDidTeleop = true;
    this.recording = new ArrayList<ArrayList<Double>>();
    if(recordAuto) {
      timerOffset = Timer.getFPGATimestamp();
    }
  }

  public void teleopPeriodic() {
    if(recordAuto) {
      double time = Timer.getFPGATimestamp() - timerOffset;
      double lx = xbox.getLeftX();
      double ly = xbox.getLeftY();
      double rx = xbox.getRightX();
      // cut off the wait at the beginning
      if(!hasBegunRecording && lx == 0 && ly == 0 && rx == 0) {
        timerOffset = Timer.getFPGATimestamp();
      } else {
        hasBegunRecording = true;
        ArrayList<Double> snapshot = new ArrayList<Double>();
        snapshot.add(time);
        snapshot.add(lx);
        snapshot.add(ly);
        snapshot.add(rx);
        recording.add(snapshot);
      }
    }
  }

  public void disabledInit() {
    hasBegunRecording = false;
    if(recordAuto && justDidTeleop) {
      justDidTeleop = false;
      // Old string: String file = "package frc.robot.autos;import java.util.ArrayList;import frc.robot.util.InputWrapper;public class NewRecording extends RecordedAuto {public NewRecording(ArrayList<InputWrapper> inputs) {super(inputs);super.points = new double[][]{";
      String file = "package frc.robot.autos;import frc.robot.subsystems.Drivetrain;public class NewRecording extends RecordedAuto {public NewRecording(Drivetrain drivetrain) {super(drivetrain);super.points = new double[][]{";
      for(ArrayList<Double> list : recording) {
        file += "{";
        for(double value : list) {
          file += value + ",";
        }
        file = file.substring(0,file.length()-1) + "},";
      }
      file = file.substring(0, file.length()-1) + "};}}";
      File f = new File("/home/lvuser/NewRecording.java");
      try {
        FileWriter fw = new FileWriter(f);
        fw.write(file);
        fw.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return new NewRecording(drivetrain);
  }
}
